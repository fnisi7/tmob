from django.core.cache import cache
from django.db.models.signals import post_save
from .models import Redirect

def cache_redirect(sender, instance, **kwargs):
    cache.set_many({o.key : o.url for o in Redirect.objects.filter(active=True)})

post_save.connect(cache_redirect, sender=Redirect)

