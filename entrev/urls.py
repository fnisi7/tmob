from django.urls import path

from . import views

urlpatterns = [
    path('get_url/<key>', views.get_url),
]