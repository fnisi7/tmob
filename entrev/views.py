from django.http import JsonResponse
from django.core.cache import cache

def get_url(request, key):
    if request.method == 'GET':
        try:
            return JsonResponse({'key':key,'url':cache.get(key,'not found')})
        # Se asume que la key siempre estará en la cache por la signal desarrollada
        except:
            return JsonResponse({'key':'not found','url':''})