from django.db import models

class Redirect(models.Model):
    key = models.CharField(max_length=100)
    url = models.CharField(max_length=100)
    active = models.BooleanField()
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)